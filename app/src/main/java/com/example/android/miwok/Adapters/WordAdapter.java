package com.example.android.miwok.Adapters;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.miwok.R;
import com.example.android.miwok.Word;

import java.util.ArrayList;


public class WordAdapter extends ArrayAdapter<Word> {

    private int mColorResourceId;


    public WordAdapter(Activity context, ArrayList<Word> words, int colorId) {
        super(context, 0, words);
        this.mColorResourceId = colorId;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }


        Word currentWord = getItem(position);


        TextView miwokTextView =  listItemView.findViewById(R.id.miwok_text_view);

        miwokTextView.setText(currentWord.getmMiwokTranslation());


        TextView defaultTextView = listItemView.findViewById(R.id.default_text_view);


        defaultTextView.setText(currentWord.getmDefaultTranslation());

        ImageView iconView = listItemView.findViewById(R.id.image_view);

        if(currentWord.hasImage()) {

            iconView.setImageResource(currentWord.getmImageId());
        }

        else {
            iconView.setVisibility(View.GONE);
        }

        View textContainer = listItemView.findViewById(R.id.text_container);

        int color = ContextCompat.getColor(getContext(), this.mColorResourceId);

        textContainer.setBackgroundColor(color);

        textContainer = listItemView.findViewById(R.id.play_button_image_view);

        textContainer.setBackgroundColor(color);

        return listItemView;
    }

}
