package com.example.android.miwok;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.miwok.Adapters.WordAdapter;

import java.util.ArrayList;

public class ColorsActivity extends AppCompatActivity {

    MediaPlayer mPlayTranslation;

    private MediaPlayer.OnCompletionListener mCompletionListenr = new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colors);

        final ArrayList<Word> colorWords = new ArrayList<Word>();

        colorWords.add(new Word("Red", "Wetetti", R.drawable.color_red,R.raw.color_red));
        colorWords.add(new Word("Green", "chokokki", R.drawable.color_green,R.raw.color_green));
        colorWords.add(new Word("Brown", "ṭakaakki", R.drawable.color_brown,R.raw.color_brown));
        colorWords.add(new Word("Gray", "ṭopoppi", R.drawable.color_gray,R.raw.color_gray));
        colorWords.add(new Word("Black", "kululli", R.drawable.color_black,R.raw.color_black));
        colorWords.add(new Word("White", "kelelli", R.drawable.color_white,R.raw.color_white));
        colorWords.add(new Word("Dusty Yellow", "ṭopiisә", R.drawable.color_dusty_yellow,R.raw.color_dusty_yellow));
        colorWords.add(new Word("Mustard Yellow", "chiwiiṭә", R.drawable.color_mustard_yellow,R.raw.color_mustard_yellow));

        WordAdapter adapter = new WordAdapter(this, colorWords, R.color.category_colors);

        ListView view = (ListView) findViewById(R.id.list);

        view.setAdapter(adapter);

        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                releaseMediaPlayer();

                mPlayTranslation = MediaPlayer.create(ColorsActivity.this,colorWords.get(position).getmAudioId());
                mPlayTranslation.start();

                mPlayTranslation.setOnCompletionListener(mCompletionListenr);
            }
        });
    }

    private void releaseMediaPlayer(){

        if(mPlayTranslation != null){
            mPlayTranslation.release();
            mPlayTranslation = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }
}
