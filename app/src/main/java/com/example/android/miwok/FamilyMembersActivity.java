package com.example.android.miwok;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.miwok.Adapters.WordAdapter;

import java.util.ArrayList;

public class FamilyMembersActivity extends AppCompatActivity {

    MediaPlayer mPlayTranslation;

    private MediaPlayer.OnCompletionListener mCompletionListenr = new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            realeaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_members);

        final ArrayList<Word> familyWords = new ArrayList<>();

        familyWords.add(new Word("father","әpә",R.drawable.family_father,R.raw.family_father));
        familyWords.add(new Word("mother","әṭa",R.drawable.family_mother,R.raw.family_mother));
        familyWords.add(new Word("son","angsi",R.drawable.family_son,R.raw.family_son));
        familyWords.add(new Word("daughter","tune",R.drawable.family_daughter,R.raw.family_daughter));
        familyWords.add(new Word("older brother","taachi",R.drawable.family_older_brother,R.raw.family_older_brother));
        familyWords.add(new Word("younger brother","chalitti",R.drawable.family_younger_brother,R.raw.family_younger_brother));
        familyWords.add(new Word("older sister","teṭe",R.drawable.family_older_sister,R.raw.family_older_sister));
        familyWords.add(new Word("younger sister","kolliti",R.drawable.family_younger_sister,R.raw.family_younger_sister));
        familyWords.add(new Word("grandmother","ama",R.drawable.family_grandmother,R.raw.family_grandmother));
        familyWords.add(new Word("grandfather","paapa",R.drawable.family_grandfather,R.raw.family_grandfather));

        WordAdapter adapter = new WordAdapter(this,familyWords,R.color.category_family);

        ListView view = (ListView) findViewById(R.id.list);

        view.setAdapter(adapter);

        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                realeaseMediaPlayer();

                mPlayTranslation = MediaPlayer.create(FamilyMembersActivity.this,familyWords.get(position).getmAudioId());
                mPlayTranslation.start();

                mPlayTranslation.setOnCompletionListener(mCompletionListenr);
            }
        });

    }

    private void realeaseMediaPlayer(){

        if(mPlayTranslation != null){
            mPlayTranslation.release();
            mPlayTranslation = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        realeaseMediaPlayer();
    }
}
