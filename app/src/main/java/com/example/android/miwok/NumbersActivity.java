package com.example.android.miwok;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.miwok.Adapters.WordAdapter;

import java.util.ArrayList;

import static android.media.CamcorderProfile.get;

public class NumbersActivity extends AppCompatActivity {

    MediaPlayer mPlayTranslation;

    private MediaPlayer.OnCompletionListener mCompletionListenr = new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            realeaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);

        //Add words for numbers

        final ArrayList<Word> wordsNumbers = new ArrayList<>();

        wordsNumbers.add(new Word("One", "Lutti", R.drawable.number_one,R.raw.number_one));
        wordsNumbers.add(new Word("Two", "Otiiko", R.drawable.number_two,R.raw.number_two));
        wordsNumbers.add(new Word("Three", "Tolookosu", R.drawable.number_three,R.raw.number_three));
        wordsNumbers.add(new Word("Four", "Oyyisa", R.drawable.number_four,R.raw.number_four));
        wordsNumbers.add(new Word("Five", "Massokka", R.drawable.number_five,R.raw.number_five));
        wordsNumbers.add(new Word("Six", "Temmokka", R.drawable.number_six,R.raw.number_six));
        wordsNumbers.add(new Word("Seven", "Kenekaku", R.drawable.number_seven,R.raw.number_seven));
        wordsNumbers.add(new Word("Eight", "Kawinta", R.drawable.number_eight,R.raw.number_eight));
        wordsNumbers.add(new Word("Nine", "Wo'e", R.drawable.number_nine,R.raw.number_nine));
        wordsNumbers.add(new Word("Ten", "Na'aacha", R.drawable.number_ten,R.raw.number_ten));


        WordAdapter adapter = new WordAdapter(this, wordsNumbers, R.color.category_numbers);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {

                realeaseMediaPlayer();

                mPlayTranslation = MediaPlayer.create(NumbersActivity.this,wordsNumbers.get(position).getmAudioId());
                mPlayTranslation.start();

                mPlayTranslation.setOnCompletionListener(mCompletionListenr);
            }
        });
    }

    private void realeaseMediaPlayer() {

        if(mPlayTranslation != null) {
            mPlayTranslation.release();
            mPlayTranslation = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        realeaseMediaPlayer();
    }
}
