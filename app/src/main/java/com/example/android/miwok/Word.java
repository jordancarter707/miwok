package com.example.android.miwok;

import android.media.MediaPlayer;

public class Word {

    private String mMiwokTranslation;
    private String mDefaultTranslation;
    private int mAudioId;

    private static final int NO_IMAGE_PROVIDED = -1;

    private int mImageId = NO_IMAGE_PROVIDED;

    public Word(String defaultLang, String miwok, int audioID){

        this.mDefaultTranslation = defaultLang;
        this.mMiwokTranslation = miwok;
        this.mAudioId = audioID;
    }

    public Word(String defaultLang, String miwok, int imageId, int audioID){

        this.mDefaultTranslation = defaultLang;
        this.mMiwokTranslation = miwok;
        this.mImageId = imageId;
        this.mAudioId = audioID;
    }

    public String getmMiwokTranslation(){

        return this.mMiwokTranslation;
    }

    public String getmDefaultTranslation(){

        return  this.mDefaultTranslation;
    }

    public int getmImageId(){

        return this.mImageId;
    }

    public int getmAudioId(){

        return this.mAudioId;
    }

    public boolean hasImage(){

        return mImageId != NO_IMAGE_PROVIDED;
    }

}
